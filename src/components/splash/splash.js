import React, { PureComponent } from "react";
import { Text, TouchableOpacity } from "react-native";

import App from "~/src/lib/app";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
// actions
import * as userActions from "~/src/redux/actions/user";

class Splash extends PureComponent {
  constructor(props) {
    super(props);
    this.onPress = this.onPress.bind(this);
  }

  componentDidMount() {
    App.navigation = this.props.navigation;
  }

  onPress() {
    this.props.actions.start_login_flow();
  }

  render() {
    return (
      <TouchableOpacity onPress={this.onPress}>
        <Text>Splash</Text>
      </TouchableOpacity>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(userActions, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Splash);
