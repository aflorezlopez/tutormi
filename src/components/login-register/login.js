import React, { PureComponent } from "react";
import 
{ 
  View, 
  Text, 
  TextInput, 
  Image, 
  TouchableOpacity,
  Button  ,

} 
  from "react-native";

import styles from "./styles";
import Link from "~/src/lib/components/link";
import Input from "~/src/lib/components/text-input/text-input";
// redux connection
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
// actions
import * as userActions from "~/src/redux/actions/user";
import App from "~/src/lib/app";


class LoginRegister extends PureComponent {
  constructor(props) {
    super(props);
    this.navigateTo = this.props.navigation.navigate;
    this.pressLoginButton = this.pressLoginButton.bind(this);
    this.navigateRegister = this.navigateRegister.bind(this);
  }
  componentDidMount() {
    App.navigation = this.props.navigation;
  }

  navigateRegister(){
    this.props.actions.start_register();
  }

  pressLoginButton() {
    this.props.actions.start_doctor_dashboard_flow();
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logo}>
                
        <Image
          style={{width: 70, height: 70}}
          source={require('./image/Logos_Tutormi.png')}
        />
        </View>
        <View style={styles.textInput}>
          <TextInput 
            style={styles.marginElement, styles.InputText}
            underlineColorAndroid="transparent"
            placeholder='Escribe tu email'
            placeholderTextColor="#ECF0F3"
            
          />
          <TextInput
            style={styles.marginElement, styles.InputText}
            underlineColorAndroid="transparent"
            placeholder='Escribe tu contraseña'
            placeholderTextColor="#ECF0F3"
            secureTextEntry={true} 
          
          />
          
          <TouchableOpacity onPress={() => {}} style={styles.buttonSession}>
             <View style = {styles.viewTouchableSession}>
                  <Text style = {styles.textTouchableSession}>INICIAR SESIÓN</Text>
                </View>
         </TouchableOpacity>

        </View>
        <View style={styles.link}>
          <Text style={styles.textQuestion}
          >
            ¿Todavía no tienes cuenta?
          </Text>
          <TouchableOpacity onPress={this.navigateRegister} style={styles.buttonSession} >
             <View>
                  <Text style = {styles.textTouchableCreate}>CREAR CUENTA</Text>
                </View>
         </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(userActions, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginRegister);
