import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent : 'space-between',
    backgroundColor : '#6B82F8'

  },
  logo : {
    marginTop : 50,
    flexDirection : 'row',
    justifyContent: 'center',
  },
 
  marginElement : {
    margin : 10,
    textAlign: 'center',

  },
  InputText : {
    borderColor: '#ECF0F3',
    color : '#ECF0F3',
    borderWidth: 1,
    margin : 10,
    borderRadius: 3,
    borderWidth: 1,

  },

  viewTouchableSession : {
    backgroundColor: '#FFF', 
    alignItems: 'center', 
    justifyContent: 'center', 
    borderRadius: 3, 
    height: 50
  },
  textTouchableSession : {
    color: '#6B82F8', 
    fontWeight: 'bold'
  },
  buttonSession : {
    margin : 10
  },
  link : {
    alignItems: 'center', 
    justifyContent: 'center', 
    marginBottom : 50,

  },
  textQuestion : {
    color : '#ECF0F3',
    marginBottom : 5
  },
  textTouchableCreate : {
    color: '#FFF', 
    fontWeight : 'bold',
    fontSize : 12,
  },


});
