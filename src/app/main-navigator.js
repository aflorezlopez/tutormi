import { createStackNavigator } from "react-navigation";

// Components
import Splash from "~/src/components/splash/splash";
import Login from "~/src/components/login-register/login";
import Register  from '~/src/components/register/register'

const RootStack = createStackNavigator(
  {
    Splash: {
      screen: Splash,
      navigationOptions: {
        title: "Splash"
      }
    },
    Login: {
      screen: Login,
      navigationOptions: {
        title: "Login"
      }
    },
    Register : {
      screen : Register,
      navigationOptions : {
        title : "Registro"
      }
    }
  },
  {
    initialRouteName: "Splash"
  }
);

export default RootStack;
