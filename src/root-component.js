import React from "react";
import MainNavigator from "~/src/app/main-navigator";
import { Provider } from "react-redux";
import store from "~/src/redux/reducers";

console.ignoredYellowBox = [
  "Warning: isMounted(...) is deprecated" // Esto es para ocultar el warning `isMounted(...) is deprecated`, que vino con
  // con la actualizacion de RN 0.55.2
  // https://github.com/react-navigation/react-navigation/issues/3956
];

const RootComponent = () => (
  <Provider store={store}>
    <MainNavigator />
  </Provider>
);

export default RootComponent;
