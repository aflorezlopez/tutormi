import { handleActions } from "redux-actions";

const initialState = {
  token: {}
};

const actionMap = {};

actionMap.RECEIVE_TOKEN = (state, action) => {
  const newState = {
    ...state,
    token: action.payload
  };
  return newState;
};

export default handleActions(actionMap, initialState);
