import thunk from "redux-thunk";
import { createAction } from "redux-actions";

import App from "~/src/lib/app";

export const start_login_flow = () => dispatch =>
  App.navigation.navigate({ routeName: "Login" });

  export const start_register = () => dispatch =>
  App.navigation.navigate({ routeName: "Register" });
