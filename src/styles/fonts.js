import { StyleSheet } from "react-native";

const fontStyle = {
  headLine: {
    fontSize: 30,
    fontWeight: "bold",
    fontFamily: "Montserrat"
  },
  headLineMedium: {
    fontSize: 25,
    fontWeight: "bold",
    fontFamily: "Montserrat"
  }
};
