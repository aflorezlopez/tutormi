import { StyleSheet } from "react-native";

const baseColors = {
  red: "rgb(244, 58, 62)",
  transparent: "transparent",
  yellow: "rgb(245, 185, 1)",
  darkGray: "rgb(76, 76, 76)",
  white: "rgb(255, 255, 255)",
  blue: "rgb(15, 90, 167)",
  paleGray: "rgb(248, 248, 248)",
  greenBlue: "rgb(34, 204, 127)",
  lightGray: "rgb(163, 163, 163)",
  salmonPink: "rgb(255, 135, 135)",
  grayOpacity: "rgba(74,74,74,0.1)",
  lightGreenBlue: "rgb(105, 218, 172)",
  whiteWithAlfa: "rgba(255, 255, 255, 0.7)"
};

const appColors = {
  mainColor: baseColors.greenBlue,
  secundaryColor: baseColors.blue
};

export default appColors;
