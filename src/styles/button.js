import { StyleSheet } from "react-native";

//styles
import colors from "./colors";

export default StyleSheet.create({
  confirmation: {
    backgroundColor: colors.mainColor,
    borderRadius: 10,
    padding: 10
  },
  confirmationText: {
    textAlign: "center"
  },
  skip: {},
  skipText: {}
});
