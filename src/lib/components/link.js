import React, { Component } from "react";
import PropTypes from "prop-types";
import { Text, TouchableOpacity, View } from "react-native";

import fonts from "~/src/styles/fonts";
import button from "~/src/styles/button";

const Link = props => (
  <TouchableOpacity onPress={() => props.onPress()}>
    <Text>{props.label}</Text>
  </TouchableOpacity>
);

Link.propTypes = {
  label: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired
};

export default Link;
