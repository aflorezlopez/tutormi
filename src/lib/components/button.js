import React, { Component } from "react";
import PropTypes from "prop-types";
import { Text, TouchableOpacity, View } from "react-native";

import fonts from "~/src/styles/fonts";
import button from "~/src/styles/button";

const TYPE = {
  CONFIRMATION: "confirmation",
  SKIP: "skip"
};

class Button extends Component {
  constructor(props) {
    super(props);
    this.buttonProperties = {
      buttonStyle: button.confirmation,
      textStyle: button.confirmationText
    };
  }

  renderButtonWithSpecificProperties() {
    return (
      <TouchableOpacity
        onPress={this.props.onPress}
        disabled={this.props.disabled}
        style={[
          this.buttonProperties.buttonStyle,
          button.basicApperence,
          this.props.disabled ? button.disableButton : button.activeButton
        ]}
      >
        <Text
          numberOfLines={1}
          style={[fonts.textButton, this.buttonProperties.textStyle]}
        >
          {this.props.label}
        </Text>
      </TouchableOpacity>
    );
  }

  renderButton() {
    if (this.props.type === TYPE.SKIP) {
      this.buttonProperties = {
        buttonStyle: button.skip,
        textStyle: button.skipText
      };
    }

    return this.renderButtonWithSpecificProperties();
  }

  render() {
    return (
      <View
        style={{
          width: this.props.size.width,
          height: this.props.size.height
        }}
      >
        {this.renderButton()}
      </View>
    );
  }
}

Button.propTypes = {
  onPress: PropTypes.func,
  size: PropTypes.shape({
    width: PropTypes.string,
    height: PropTypes.string
  }),
  type: PropTypes.string,
  disabled: PropTypes.bool,
  label: PropTypes.string.isRequired
};

Button.defaultProps = {
  onPress: () => {},
  size: {
    width: "100%",
    height: "100%"
  },
  type: TYPE.CONFIRMATION,
  disabled: false
};

export default Button;
