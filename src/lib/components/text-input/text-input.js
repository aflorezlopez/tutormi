import React, { PureComponent } from "react";
import { TextInput } from "react-native";
import PropTypes from "prop-types";
import styles from "./styles";

class Input extends PureComponent {
  render() {
    return (
      <TextInput
        keyboardType={this.props.keyboardType}
        underlineColorAndroid="rgba(0, 0, 0, 0)"
        placeholder={this.props.placeholder}
        style={this.props.styles}
      />
    );
  }
}

Input.propTypes = {
  placeholder: PropTypes.string,
  keyboardType: PropTypes.string,
  isActive: PropTypes.bool,
  placeholderTextColor: PropTypes.string,
  styles: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number,
    PropTypes.array
  ])
};

Input.defaultProps = {
  keyboardType: "default",
  styles: styles.textInput
};

export default Input;
