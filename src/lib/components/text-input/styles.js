import { StyleSheet } from "react-native";

export default StyleSheet.create({
  textInput: {
    width: "80%",
    height: "18%",
    justifyContent: "center",
    paddingLeft: "5%",
    borderBottomWidth: 1,
    marginBottom: "3%",
    borderColor: "grey"
  }
});
