## Folders

- Components live at `src/components/*`
- Applications code (business logic) lives at `src/app/*`
- Code that might be reused across Essentials implementations lives at `src/lib/*`
- Actions live at `src/redux/actions/*`
- Actions with side effects live at `src/redux/middlewares/*`
- Reducers live at `src/redux/reducers/*`

## Components

- Components should only take care of: Displaying logic, animations, triggering actions
- All component's props need to fall in the following categories with the following name patterns:
  - Data sent from parent components: Names should be descriptive of what the data is (not what is used for)
  - Data from the redux state: Names should be descriptive of what the data is (not what is used for), using the same or a similar name from the redux state
  - Functions that need to be executed in the parent component when X thing happend: Names should start "on", like `onTextChanged` and should describe what the component (NOT the parent coponent) did, sort of like an event.
- Keep re-renders at a minimum
- All texts should be coming out of `I18n.t("potato.potato")`
- Styles will not be inlined in the `render` funcion but will be in a `Style` object
- Components' only side effects can be actions. They only need to trigger actions that through the middleware make side effects
- Components need to specify their `propTypes` by doing `Component.propTypes = {}` and speciying all types and required props
- Animations should start from a method called `bootAnimations()`
- All animations should live within an object `this.animations`
- For infinite animations, use `Animated.loop(animation).start();` so that components can be fully tested
- Device properties should be coming out of `Device.*`
